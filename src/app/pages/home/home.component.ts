import { Component, OnInit } from '@angular/core';
import { MovieModel } from 'src/app/models/movie.model';
import { TvModel } from 'src/app/models/tv.model';
import { MovieService } from 'src/app/services/movie.service';
import { TvshowService } from 'src/app/services/tvshow.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private movieService: MovieService, private tvService: TvshowService) {}

  imgBase = 'https://image.tmdb.org/t/p/w500';
  movies: MovieModel[] = [];
  tvShows: TvModel[] = [];


  caroselImages = [
    { path: '../../../assets/images/1.jpg' },
    { path: '../../../assets/images/2.jpg' },
    { path: '../../../assets/images/3.jpg' },
  ];

  ngOnInit(): void {
    this.getMovies();
    this.getTVShows();
  }

  getMovies() {
    this.movieService.getMovies().subscribe((data) => {
      this.movies = data;
    });
  }

  getTVShows() {
    this.tvService.getTvShows().subscribe((data) => {
      this.tvShows = data;
    });
  }
}
