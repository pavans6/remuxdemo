import { Routes } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { MovieComponent } from '../pages/movie/movie.component';
import { TvshowComponent } from '../pages/tvshow/tvshow.component';


export const LayoutRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
      path: 'movies',
      component: MovieComponent
  },
  {
      path: 'tv-shows',
      component: TvshowComponent
  }
];
