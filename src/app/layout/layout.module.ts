import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LayoutRoutes } from './layout.routing';
import { HomeComponent } from '../pages/home/home.component';
import { MovieComponent } from '../pages/movie/movie.component';
import { TvshowComponent } from '../pages/tvshow/tvshow.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LayoutRoutes),
    IvyCarouselModule
  ],
  declarations: [
    HomeComponent,
    MovieComponent,
    TvshowComponent
  ],
})
export class LayoutModule {}
