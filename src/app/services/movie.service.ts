import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MovieModel } from '../models/movie.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  constructor(private http: HttpClient) {}

  baseUrl = 'https://api.themoviedb.org/3/movie/popular';

  getMovies(): Observable<MovieModel[]> {
    return this.http
      .get<MovieModel[]>(this.baseUrl, {
        params: {
          api_key: environment.apiKey,
          language: 'en-US',
          page: '1'
        },
      })
      .pipe(
        map((data) => {
          return data;
        })
      );
  }
}
