import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TvModel } from '../models/tv.model';

@Injectable({
  providedIn: 'root',
})
export class TvshowService {
  constructor(private http: HttpClient) {}

  baseUrl = 'https://api.themoviedb.org/3/tv/popular';

  getTvShows(): Observable<TvModel[]> {
    return this.http
      .get<TvModel[]>(this.baseUrl, {
        params: {
          api_key: environment.apiKey,
          language: 'en-US',
          page: '1',
        },
      })
      .pipe(
        map((data) => {
          return data;
        })
      );
  }
}
