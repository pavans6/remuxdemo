class Tv {
    id: number;
    title: string;
    backdrop_path: string;
}
export class TvModel {
    page: number;
    results: Tv[] = [];
}
