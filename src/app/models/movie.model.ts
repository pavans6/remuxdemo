class Movie {
    id: number;
    title: string;
    backdrop_path: string;
}
export class MovieModel {
    page: number;
    results: Movie[] = [];
}
